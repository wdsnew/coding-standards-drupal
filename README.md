# Smartbees Coding Standards for Drupal based projects

```sh
# Install coding standards.
composer req --dev smartbees/coding-standards-drupal
vendor/bin/sbcs init

# Add 'sbcs init' to the 'post-update-cmd' scripts in the composer.json file
# to automatically install the coding standards configs updates when they are
# available.
...
"scripts": {
    "post-update-cmd": [
        "vendor/bin/sbcs init"
    ],
...

# Run static analysis of the code.
vendor/bn/sbcs check

# Execute automatic code style fixes.
vendor/bin/sbcs check --fix
```
