<?php

// Load the default configuration.
$config = new TwigCsFixer\Config\Config();

// Enable reporting of non-fixable rules.
$config->allowNonFixableRules();

// Add drupal/core translation token parsers.
$config->addTokenParser(new Drupal\Core\Template\TwigTransTokenParser());

// Add drupal/storybook twig extension.
if (class_exists('TwigStorybook\Twig\TwigExtension') && class_exists('TwigStorybook\Service\StoryCollector')) {
  $drupalRoot = 'web';
  $storyCollector= new TwigStorybook\Service\StoryCollector();
  $config->addTwigExtension(new TwigStorybook\Twig\TwigExtension($storyCollector, $drupalRoot));
}

// Load default ruleset.
$ruleset = new TwigCsFixer\Ruleset\Ruleset();

// Add Drupal specific rules.
$ruleset->addRule(new TwigCsFixerDrupal\Rules\Component\RequireComponentAttributesRule());

$config->setRuleset($ruleset);

return $config;
