<?php

declare(strict_types=1);

namespace Smartbees\CodingStandards\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Create basic configs for code analysis tools.
 */
class InitCommand extends Command {

  /**
   * Configs dir.
   */
  protected const CONFIGS_DIR = __DIR__ . '/../../configs';

  /**
   * {@inheritdoc}
   */
  protected function configure(): void {
    $this->setName('init');
    $this->setDescription('Creates basic configs for code analysis tools.');
    $this->addArgument('path', InputArgument::OPTIONAL, 'Root path of the project.', getcwd());
  }

  /**
   * {@inheritdoc}
   */
  public function execute(InputInterface $input, OutputInterface $output): int {
    $this->copyConfigs($input->getArgument('path'), self::CONFIGS_DIR);
    return 0;
  }

  /**
   * Copy configs to destination path.
   */
  protected function copyConfigs(string $dst, string $src): void {
    $dir = opendir($src);
    @mkdir($dst);
    while ($file = readdir($dir)) {
      if (($file != '.') && ($file != '..')) {
        if (is_dir($src . '/' . $file)) {
          $this->copyConfigs($dst . '/' . $file, $src . '/' . $file);
        }
        else {
          copy($src . '/' . $file, $dst . '/' . $file);
        }
      }
    }
    closedir($dir);
  }

}
